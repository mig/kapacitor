# do not strip the binary
%global debug_package %{nil}
%global __os_install_post %{nil}

Name:    kapacitor
Version: 1.7.6
Release: 1%{?dist}
Summary: Time series data processing engine
Group:   System Environment/Daemons
License: MIT
URL:     https://www.influxdata.com/time-series-platform/kapacitor/
Source0: kapacitor-%{version}-1.x86_64.rpm
Source1: kapacitor-%{version}-1.aarch64.rpm

%description
Time series data processing engine

%prep
%ifarch x86_64
rpm2cpio %{SOURCE0} | cpio -idum
%endif
%ifarch aarch64
rpm2cpio %{SOURCE1} | cpio -idum
%endif

%build
rm -fr usr/lib/.build-id

%install
rm -fr ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}
mv * ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root,-)
%dir /etc/kapacitor
%config(noreplace) /etc/kapacitor/*
%config(noreplace) /etc/logrotate.d/*
/usr/bin/*
/usr/lib/kapacitor
/usr/share/bash-completion/completions/*
%dir %attr(0755, kapacitor, kapacitor) /var/lib/kapacitor
%dir %attr(0755, kapacitor, kapacitor) /var/log/kapacitor

%pre
if ! id kapacitor >/dev/null 2>&1; then
  useradd --system -U -M kapacitor -s /bin/false -d /var/lib/kapacitor
fi

%post
test -f /etc/default/kapacitor || touch /etc/default/kapacitor
cp -f /usr/lib/kapacitor/scripts/kapacitor.service /lib/systemd/system/kapacitor.service
systemctl daemon-reload

%postun
if [[ "$1" = "0" ]]; then
  rm -f /etc/default/kapacitor
  systemctl disable kapacitor
  rm -f /lib/systemd/system/kapacitor.service
fi
